package morpion.jeu;

import java.util.Scanner;

/**
 *
 * @author Charles Garoux
 */
public class Morpion {
    
    public static void main(String[] args)
    {
	int turn;
	int round;
	int nbRounds;
	char piece;
	String name;
	char choice;
	Scanner Keyboard = new Scanner(System.in);
	Player Player1 = new Player();
	Player Player2 = new Player();
	
	System.out.println("Default players or custome  players?[input any key for default player or 'c' for custom player]");
	choice = inputChar();
	if (choice == 'c')
	{
	    System.out.println("Input the name of Player1:");
	    name = Keyboard.next();
	    System.out.println("Now input the piece you want:");
	    piece = inputChar();
	    Player1.setNameAndPiece(name, piece);
	    System.out.println(Player1.getName());
	    
	    System.out.println("Input the name of Player2:");
	    name = Keyboard.next();
	    System.out.println("Now input the piece you want:");
	    piece = inputChar();
	    Player2.setNameAndPiece(name, piece);
	}
	else
	{
	    Player1.setNameAndPiece("Player One", 'O');
	    Player2.setNameAndPiece("Player Two", 'X');	
	}
	
	System.out.println("How many games do you want to play?");
	nbRounds = Keyboard.nextInt();
	
	BoardGame Board = new BoardGame();
	System.out.println("The game start, good luck!");
	turn = 0;
	round = 0;
	while(round < nbRounds)
	{
	    while(true)
	    {
		Board.printBoardGame();
		System.out.println("It's your turn " + Player1.getName() + " chose a case to put your piece:");
		Board.putThePiece(Player1.getPiece(), inputChar());
		turn++;
		Board.printBoardGame();
		if(checkForVictory(Board, Player1, Player2) == 1)
		    break;
		if (turn == 9)
		{
		    System.out.println("Equality!");
		    break;
		}
		System.out.println("It's your turn " + Player2.getName() + " chose a case to put your piece:");
		Board.putThePiece(Player2.getPiece(), inputChar());
		turn++;
		if(checkForVictory(Board, Player1, Player2) == 1)
		    break;
	    }
	    Board.resetBoard();
	    round++;
	}
	System.out.println("Scores:\n"
			 + "Player 1: " + Player1.getScore() +"\n"
			 + "Player 2: " + Player2.getScore()	    );
    }
    
    private static int checkForVictory(BoardGame Board, Player Player1, Player Player2)
    {
	if(Board.whichPieceWins() == Player1.getPiece())
	{
	    System.out.println(Player1.getName() + " win the game!");
	    Player1.addPointToScore();
	    return 1;
	}
	else if (Board.whichPieceWins() == Player2.getPiece())
	{
	    System.out.println(Player2.getName() + " win the game!");
	    Player2.addPointToScore();
	    return 1;
	}
	return 0;
    }
    
    public static char inputChar()
    {
	char c;
	Scanner Key = new Scanner(System.in);
	c = Key.next().charAt(0);	    //take juste the first char of the string
	return c;
    }
}
